package com.example.app3.model


import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Oilshell(
    @StringRes val stringResourcePrice: Int,
    @StringRes val stringResourceOil: Int,
)

